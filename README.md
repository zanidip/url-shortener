# Presentation

This is a url-shortener tool :
- Ask a shorten url with a POST request :
```bash
$ curl -X POST -d '{ "url": "http://test.me" }' url-shortener-url/newurl

{"url":"http://test.me","shortenUrl":"3Ea4oh7rJ"}
```
- Access your url with the short link :
```bash
$ curl -i -X GET url-shortener-url/3Ea4oh7rJ

HTTP/1.1 302 Found
<a href="http://test.me">Found</a>.
```

---

This tool is :
 - written in golang
 - packed with packer
 - using a hosted DynamoDB
 - provisioned with terraform on AWS
 - scalable with auto-scaling group 

## diagram
```plantuml
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.1.0
skinparam defaultTextAlignment center

!include ICONURL/common.puml
!include ICONURL/font-awesome-5/sitemap.puml
!include ICONURL/font-awesome-5/database.puml
!include ICONURL/font-awesome-5/aws.puml

FA5_SITEMAP(lb,Load Balancer,node) #White
FA5_AWS(instance1,EC2 instance + docker,node)        #White
FA5_AWS(instance2,EC2 instance + docker,node)        #White
FA5_DATABASE(database,DynamoDB database,node)   #White

lb ..> instance1
lb ..> instance2

instance1 ..> database
instance2 ..> database
```

# prerequisite

 - Golang

install it, set up GOPATH/GOBIN correctly

 - Terraform v0.12

setup your access key to aws in ~/.aws/credentials and the region in ~/.aws/config

 - docker

# Prepare database

- create the DB (name `Urls`, hash key `Short`) with terraform :

```bash
cd terraform
terraform init
terraform apply -target aws_dynamodb_table.dynamodb-table
```

# Main program

the program listen to 3 endpoint :
 - `/` return a `status:ok` code for healthcheck 
 - `/new_url` will write in the DB the link with a new short-url and return the short link
 - `/<short-link>` will try to get the link associated to the short url and send redirection

## Local build 
```bash
cd golang
go get
go run main.go
```

## Test

```bash
curl -i -X POST -d '{ "url": "http://test.me" }' localhost:8080/newurl
curl -i -X GET localhost:8080/<returned shortlink> 
```
or run `./test.sh` that will test it on localhost:8080


# dockerise the app

We use multi-stage build to create a portable and lightweight image. Build is made from official golang image, the app is packed on alpine image.

## pack the app :
```bash
cd /golang
docker build -t registry.gitlab.com/zanidip/url-shortener/main
docker push registry.gitlab.com/zanidip/url-shortener/main
```

## test the image :

```bash
docker run -it --rm --name myapp \
    -v ~/.aws:/root/.aws \
    -p 8080:8080 \
    registry.gitlab.com/zanidip/url-shortener/main 
```
Check program with previous commands

# Terraform provisioning

Update variables in vars.tf (your public IP for SSH, name of your AWS ssh key)

We use an AMI created from Official ubuntu 18. Docker is installed on top to allow service to start faster.

we use autoscalling group and elastic load balancer that allow us to have :
 - easy scalability
 - zero down time deployments
 - auto redeployment of unhealthy instances

To provision :

```bash
cd terraform
terraform init
terraform plan
terraform apply
```
Expected output :
```
(...)
aws_autoscaling_group.asg: Creation complete after 44s [id=tf-asg-20191202125657267600000002]

Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

Outputs:
dns_name = load-balancer-1234567.ap-southeast-1.elb.amazonaws.com

```

Use the returned dns_name to test the app (its propagation can take some time)

```
curl -i -X POST -d '{ "url": "http://test.me" }' load-balancer-1234567.ap-southeast-1.elb.amazonaws.com/newurl
curl -i -X GET load-balancer-1234567.ap-southeast-1.elb.amazonaws.com/<returned shortlink> 
```

## Scale it

 - Change variables `asg_min_size` and `asg_max_size`.
 - Run terraform apply.

## Debug :

SSH to the instance, get the log from the container

# Ideas of amelioration 
 - Allow the program to run in a mocked database for testing
 - handle exceptions in a better way
 - automated testing and deployment
   - with gitlab-ci ?
 - improve health checks (test db connexion before answering ok)
 - put terraform remote state in S3