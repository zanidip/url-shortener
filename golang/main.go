package main
 
import (
    "fmt"
    "log"
    "net/http"
    "io/ioutil"
    "os"
    "math/rand"
    "github.com/gorilla/mux"
    "encoding/json"
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/dynamodb"
    "github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
    "time"
)

// name of the dynamodb table
var tableName string = "Urls"
// character used to form short links
var letter = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// database structure
type Item struct {
    Short  string
    Long   string
}

// http answer structure for new url
type Answer struct { 
    Url         string  `json:"url"` 
    ShortenUrl  string  `json:"shortenUrl"` 
}

// Generate random string with <n> lengh
func RandomString(n int) string { 
	b := make([]rune, n)
	for i := range b { b[i] = letter[rand.Intn(len(letter))] }
	return string(b)
}


// Write association [ short_link -> saved_link ] in dynamodb
func write_table(svc *dynamodb.DynamoDB, short_link string, saved_link string) {

    // format data
    item := Item{
        Short: short_link,
        Long:  saved_link,
    }

    av, err := dynamodbattribute.MarshalMap(item)
    if err != nil {
        fmt.Println("Got error marshalling Url data :")
        fmt.Println(err.Error())
    }

    // prepare input
    input := &dynamodb.PutItemInput{
        Item:      av,
        TableName: aws.String(tableName),
    }

    // Write in DB
    _, err = svc.PutItem(input)
    if err != nil {
        fmt.Println("Got error calling PutItem:")
        fmt.Println(err.Error())
        os.Exit(1)
    }

    fmt.Println("Saved entry for " + saved_link + " with shorten url : " + short_link)
}


// read saved_link from short_link in dynamodb
func read_table(svc *dynamodb.DynamoDB, short_link string) string {

    // Try to get item
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Short": {
				S: aws.String(short_link),
			},
		},
	})

    if err != nil {
		fmt.Println(err.Error())
		return "DB-error"
	}

	item := Item{}

    //very basic exeption handeling
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}

	if item.Short == "" {
		fmt.Println("Could not find link with ID ", short_link )
		return "Not-Found"
	}

	fmt.Println("Found item Url ", item.Long, " for key" , short_link)
    return item.Long
}


func new_url(w http.ResponseWriter, r *http.Request, svc *dynamodb.DynamoDB) {
    
    // read body to get the url
    reqBody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        log.Fatal(err)
    }

    // parse json
    var data map[string]interface{}
    error := json.Unmarshal([]byte(reqBody), &data)
    if error != nil {
        panic(error)
    }
    saved_link := fmt.Sprintf("%v", data["url"])

    fmt.Println("New url received ", saved_link)

    // create short link
    short_url := RandomString(9)

    // write table with key > link
    write_table(svc, short_url , saved_link)

    // prepare json and return it
    w.Header().Set("Content-Type", "application/json") 
    answer := Answer {
        Url:        saved_link, 
        ShortenUrl: short_url,
    } 
    json.NewEncoder(w).Encode(answer) 
}




func get_url(w http.ResponseWriter, r *http.Request, svc *dynamodb.DynamoDB) {

    // receive short_url
    vars := mux.Vars(r)
    short_url := vars["short_url"]

    // Get associated link in DB 
    saved_url := read_table(svc , short_url)

    // super basic error handeling
    if saved_url == "Not-Found" {
        fmt.Fprintf(w, "key Not Found")
        return
    }

    // print some log to stdout
    fmt.Println(" -> Called get function , looking for url ", short_url, ", found : " + saved_url)

    // send redirect to saved_link
    http.Redirect(w,r,saved_url,302)


}



func main() {

    // generate seed for random short url generation
    rand.Seed(time.Now().UnixNano())

    // Create DynamoDB client. We'll create the session once and give it to other functions as a pointer.
    sess := session.Must(session.NewSessionWithOptions(session.Options{
        SharedConfigState: session.SharedConfigEnable,
    }))
    svc := dynamodb.New(sess)

    // Create http listeners
    router := mux.NewRouter().StrictSlash(true)

    // for aws helthcheck
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(map[string]bool{"ok": true})
    })
    
    router.HandleFunc("/newurl",   func(w http.ResponseWriter, r *http.Request) { new_url(w, r, svc)}).Methods("POST")
    router.HandleFunc("/{short_url}", func(w http.ResponseWriter, r *http.Request) { get_url(w, r, svc)}).Methods("GET")

    fmt.Println("program started")

    log.Fatal(http.ListenAndServe(":8080", router))
}
