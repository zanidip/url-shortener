resource "aws_dynamodb_table" "dynamodb-table" {
  name           = var.table_name
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "Short"

  attribute {
    name = "Short"
    type = "S"
  }

}