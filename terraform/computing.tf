
resource "aws_launch_configuration" "lc" {
  image_id        = var.image_id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.instances.id]
  key_name        = var.key_name
  iam_instance_profile = aws_iam_instance_profile.instances_iam.id
  user_data = <<-EOF
              #!/bin/bash
              docker run -d \
                  -p 8080:8080 \
                  -e AWS_DEFAULT_REGION=ap-southeast-1 \
                  ${var.docker_image}
              EOF
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  launch_configuration = aws_launch_configuration.lc.id
  availability_zones = ["ap-southeast-1a"]
  min_size = var.asg_min_size
  max_size = var.asg_max_size

  load_balancers    = [aws_elb.elb.name]
  health_check_type = "ELB"

  tag {
    key                 = "Name"
    value               = "terraform-asg"
    propagate_at_launch = true
  }
}

resource "aws_elb" "elb" {
  name               = "load-balancer"
  availability_zones = ["ap-southeast-1a"]

  health_check {
    target              = "HTTP:8080/"
    interval            = 30
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
}