variable "image_id" {
  type = string
  default = "ami-0d322f4e99bc3c901"
}

variable "table_name" {
  type = string
  default = "Urls"
}

variable "key_name" {
  type = string
  default = ""
}

variable "authorized_ips"  {
  type    = list(string)
  default = ["1.1.1.1/32"]
}

variable "asg_min_size"  {
  default = 2
}

variable "asg_max_size"  {
  default = 2
}

variable "docker_image"  {
  type = string
  default = "registry.gitlab.com/zanidip/url-shortener/main"
}
