provider "aws" {
  region = "ap-southeast-1"
}



output "dns_name" {
  value       = aws_elb.elb.dns_name
  description = "The domain name of our service"
}

