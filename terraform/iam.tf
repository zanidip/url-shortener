resource "aws_iam_instance_profile" "instances_iam" {
  name = "urlshortener_profile"
  role = aws_iam_role.iam_role.name
}

resource "aws_iam_role" "iam_role" {
  name = "iam_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow"
        },
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "dynamodb.amazonaws.com"
            },
            "Effect": "Allow"
        }
    ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "attach_dynamo" {
  role       = aws_iam_role.iam_role.name
  policy_arn = aws_iam_policy.dynamo_policy.arn
}

resource "aws_iam_policy" "dynamo_policy" {
  name        = "dynamo-policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListAndDescribe",
            "Effect": "Allow",
            "Action": [
                "dynamodb:List*",
                "dynamodb:DescribeReservedCapacity*",
                "dynamodb:DescribeLimits",
                "dynamodb:DescribeTimeToLive"
            ],
            "Resource": "*"
        },
        {
            "Sid": "SpecificTable",
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": "arn:aws:dynamodb:*:*:table/Urls"
        }
    ]
}
EOF
}

